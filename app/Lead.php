<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    protected $fillable = ['car', 'name', 'surname', 'rut', 'telephone', 'commune', 'email'];
}

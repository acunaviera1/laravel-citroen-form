<?php

namespace App\Http\Controllers;

use App\Lead;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\LeadRequest;
use Mail;

class LeadController extends Controller
{

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LeadRequest $request)
    {
        $lead = new Lead($request->all());
        $lead->save();
        Mail::send('emails/notifycontact',
        array(
            'car' => $request->get('car'),
            'name' => $request->get('name'),
            'surname' => $request->get('surname'),
            'rut' => $request->get('rut'),
            'telephone' => $request->get('telephone'),
            'commune' => $request->get('commune'),
            'email' => $request->get('email')
        ), function($message)
        {
            $message->from('dev.lacomarca@gmail.com', 'Dev La Comarca');
            $message->bcc('dev.lacomarca@gmail.com', 'Dev La Comarca inbox');
            $message->to(['callcenter@citroen.cl','xmiranda@citroen.cl'])->subject('Citroën Contacto Banner 2');
            // $message->to('alex.acuna@grupocopesa.cl')->subject('Citroën Contacto Banner 2');
        }
    );
        return response()->json(['status' => 'OK']);
    }

}

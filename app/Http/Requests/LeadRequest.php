<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LeadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'car' => 'max:80',
            'name' => 'required | max:80',
            'surname' => 'required | max:80',
            'rut' => 'required | max:13',
            'telephone' => 'required | max:18',
            'commune' => 'max:30',
            'email' => 'required | email | max:80 '
        ];
    }
    public function messages()
    {
        return [
            'car.required' => 'Se necesita el modelo del auto',
            'name.required' => 'Se necesita el nombre',
            'surname.required' => 'Se necesita el apellido',
            'rut.required' => 'Se necesita el RUT',
            'telephone.required' => 'Se necesita el teléfono',
            'commune.required' => 'Se necesita la comuna',
            'email.required' => 'Se necesita el email'
        ];
    }
}

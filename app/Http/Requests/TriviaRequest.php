<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TriviaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required | email | max:100 '
        ];
    }
    public function messages()
    {
        return [
            'email.required' => 'Se necesita el email',
            'email.email' => 'El email es inválido',
            'email.max' => 'El email excede el máximo permitido',
        ];
    }
}
